package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"

	"github.com/emirpasic/gods/trees/redblacktree"
	treeutils "github.com/emirpasic/gods/utils"
)

var (
	filename string
	n        int
	workers  int

	wg = sync.WaitGroup{}
)

func init() {
	flag.StringVar(&filename, "filename", "topN.txt", "filename to open for numbers")
	flag.IntVar(&n, "n", 10, "top numbers to return")
	flag.IntVar(&workers, "workers", 2, "number of workers to use")

	flag.Parse()
}

// trackNums will track the a set of numbers inside of the file provided by filename,
// it will chunk the file from start to end provided by the caller. The numbers are stored
// inside of a red-black tree so that additions and inserts are efficient (logn).
func trackNums(needed int, filename string, start, end int64, results chan []interface{}) {
	var (
		text string
		num  int64
		err  error
		node *redblacktree.Node

		smallest = int64(0)
		rbtree   = redblacktree.NewWith(treeutils.Int64Comparator)
	)

	defer wg.Done()

	f, err := os.Open(filename)
	if err != nil {
		log.Fatalf("unable to open %s: %+v", filename, err)
	}
	defer f.Close()

	reader := io.NewSectionReader(f, start, end)
	scanner := bufio.NewScanner(reader)

	// Throw away the first line since we can't be sure that we're reading an entire line
	// unless we started at the beginning of the file where start == 0
	if start != 0 {
		scanner.Scan()
	}

	for scanner.Scan() {
		text = scanner.Text()

		num, err = strconv.ParseInt(strings.TrimSpace(text), 10, 64)
		if err != nil {
			continue
		}

		// Only do an addition when we have a value that is larger than the current smallest
		if num > smallest {

			// Add to the tree. We add the number as both the key and value in the key
			rbtree.Put(num, num)

			// check the size of the tree. If larger than what we need, prune the smallest which is the
			// left-most node in the tree and get the new smallest number to use for this check
			if rbtree.Size() > needed {
				node = rbtree.Left()
				rbtree.Remove(node.Key)
				node = rbtree.Left()
				smallest = node.Key.(int64)
			}
		}
	}

	// We return just the keys as a slice to the main function that will find the top values from all the
	// go routines
	results <- rbtree.Keys()
}

func main() {
	resultsChan := make(chan []interface{})

	f, err := os.Stat(filename)
	if err != nil {
		log.Fatalf("unable to stat %s: %+v", filename, err)
	}

	fileSize := f.Size()
	chunkSize := fileSize % int64(workers)

	// Start the workers and chunk up the file for reading
	for i := 0; i < workers; i++ {
		//go trackNums(n, workerChan, resultsChan)
		startChunk := chunkSize * int64(i)
		// Add some extra reading to the end to make sure we are able to read every line
		lastChunk := (startChunk * chunkSize) + 1024
		if i == workers-1 {
			lastChunk = fileSize
		}

		go trackNums(n, filename, startChunk, lastChunk, resultsChan)
		wg.Add(1)
	}

	// Create a new red-black tree and add in the results returns from the workers
	tree := redblacktree.NewWith(treeutils.Int64Comparator)
	for i := 0; i < workers; i++ {
		results := <-resultsChan
		for _, v := range results {
			num := v.(int64)
			tree.Put(num, num)
		}
	}

	// Pull out the highest values inside of the red-black tree by pulling out n from the right
	// side of the tree. Each time we pull out a value we remove it from the tree to help prune
	// the highest value and to get the next highest value.
	for i := 0; i < n; i++ {
		node := tree.Right()
		fmt.Printf("%d ", node.Key.(int64))
		tree.Remove(node.Key)
	}
}
