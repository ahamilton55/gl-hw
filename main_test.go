package main

import (
	"os"
	"testing"
)

func TestTrackNums(t *testing.T) {
	tests := []struct {
		name     string
		filename string
		n        int
		expected []int64
	}{
		{
			name:     "ascending_single",
			filename: "test_files/ascending.txt",
			n:        5,
			expected: []int64{96, 97, 98, 99, 100},
		},
		{
			name:     "descending",
			filename: "test_files/descending.txt",
			n:        5,
			expected: []int64{96, 97, 98, 99, 100},
		},
		{
			name:     "random",
			filename: "test_files/random.txt",
			n:        5,
			expected: []int64{4967641158728, 4973620127957, 4981295831968, 4989143553902, 4998589885484},
		},
	}

	results := make(chan []interface{})

	for _, test := range tests {
		t.Run(test.name, func(tt *testing.T) {
			f, err := os.Stat(test.filename)
			if err != nil {
				tt.Fatalf("unable to open test file %s: %+v", test.filename, err)
			}

			wg.Add(1)
			go trackNums(test.n, test.filename, 0, f.Size(), results)

			values := <-results

			for i, v := range values {
				num := v.(int64)
				if test.expected[i] != num {
					tt.Fatalf("unexpected value returned, expected %d, got %d", test.expected[i], num)
				}
			}
		})
	}
}
